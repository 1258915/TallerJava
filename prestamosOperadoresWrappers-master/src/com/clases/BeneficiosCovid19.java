package com.clases;

public class BeneficiosCovid19 {
    //Debe ser el numero entero aleatorio puedes usar la funcion ramdom para esto
    private String id;
    private String nombre;
    private Float valorSubsidio;

    public BeneficiosCovid19(String id, String nombre, Float valorSubsidio) {
        this.id = id;
        this.nombre = nombre;
        this.valorSubsidio = valorSubsidio;
    }

    public String getId() {
        return String.valueOf ( Integer.valueOf ( id ) );
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Float getValorSubsidio() {
        return valorSubsidio;
    }

    public void setValorSubsidio(Float valorSubsidio) {
        this.valorSubsidio = valorSubsidio;
    }

    //Completar clase

    //Implementar un metodo que retorne el mejor beneficio comparando los valores de todos los beneficios
    public Float getMejorBeneficios(){
        return Float.valueOf(0);
    }
}
