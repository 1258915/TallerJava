package clases;

public class Constantes {

    public static final String INGMENSUALES = "CUALES SON SUS INGRESOS MENSUALES";
    public static final String GASTOSFIJOS = "INGRESE GASTOS FIJOS";
    public static final String GASTOSVARIABLES = "INGRESE GASTOS VARIABLES";
    public static final String CONTLETRAS = "VALOR NO VALIDO, CONTIENE LETRAS";
    public static final String CAPACIDAD = "SU CAPACIDAD DE ENDEUDAMIENTO ES: ";

    public Constantes() {
    }

}
