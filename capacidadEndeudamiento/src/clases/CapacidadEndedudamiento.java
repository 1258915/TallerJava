package clases;

public class CapacidadEndedudamiento {
    Integer ingresosTotales;
    Integer gastosFijos;
    Integer gastosVariables;
    final double POR_FIJO = 0.35;

    public CapacidadEndedudamiento(Integer ingresosTotales, Integer gastosFijos, Integer gastosVariables) {
        this.ingresosTotales = ingresosTotales;
        this.gastosFijos = gastosFijos;
        this.gastosVariables = gastosVariables;
    }

    public Integer getIngresosTotales() {
       return ingresosTotales;
    }

    public Integer getGastosFijos() {
        return gastosFijos;
    }

    public Integer getGastosVariables() {
        return gastosVariables;
    }

    public double getPOR_FIJO() {
        return POR_FIJO;
    }

    public void setIngresosTotales(Integer ingresosTotales) {
        this.ingresosTotales = ingresosTotales;
    }

    public void setGastosFijos(Integer gastosFijos) {
        this.gastosFijos = gastosFijos;
    }

    public void setGastosVariables(Integer gastoVaribales) {
        this.gastosVariables = gastoVaribales;
    }

    //Construye un metodo que retorne una cadena con las propiedades de la clase

    public Double getCapacidadEndeudamiento() {
        // retornar la capacidad de endeudamiento puede ser una cadena con el valor
        double capacidadEndeudamiento = (getIngresosTotales () - (getGastosFijos () + getGastosVariables ())) * getPOR_FIJO ();
        return capacidadEndeudamiento;
    }

    public void mostrarCapacidad() {
        double capacidadEndeudamiento = getCapacidadEndeudamiento ();
        System.out.println ( Constantes.CAPACIDAD + capacidadEndeudamiento );
    }
}
