package com.company;

import clases.CapacidadEndedudamiento;
import clases.Constantes;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int ingMensuales1 = 0;
        int gastosFijos1 = 0;
        int gastosVariables1 = 0;
        Scanner in = new Scanner ( System.in );

        String entrada = "SI";
        while (entrada.equals ( "SI" )) {
            System.out.println ( Constantes.INGMENSUALES );
            String ingMensuales = in.nextLine ();
            if (isNumeric ( ingMensuales )) {
                ingMensuales1 = Integer.parseInt ( ingMensuales );
                entrada = "NO";
            } else {
                System.out.println ( Constantes.CONTLETRAS );
            }
        }

        entrada = "SI";
        while (entrada.equals ( "SI" )) {
            System.out.println ( Constantes.GASTOSFIJOS );
            String gastosFijos = in.nextLine ();
            if (isNumeric ( gastosFijos )) {
                gastosFijos1 = Integer.parseInt ( gastosFijos );
                entrada = "NO";
            } else {
                System.out.println ( Constantes.CONTLETRAS );
            }
        }

        entrada = "SI";
        while (entrada.equals ( "SI" )) {
            System.out.println ( Constantes.GASTOSVARIABLES );
            String gastosVariables = in.nextLine ();
            if (isNumeric ( gastosVariables )) {
                gastosVariables1 = Integer.parseInt ( gastosVariables );
                entrada = "NO";
            } else {
                System.out.println ( Constantes.CONTLETRAS );
            }
        }

        CapacidadEndedudamiento capacidadEndedudamiento = new CapacidadEndedudamiento ( ingMensuales1,
                gastosFijos1, gastosVariables1 );
        capacidadEndedudamiento.getCapacidadEndeudamiento ();
        capacidadEndedudamiento.mostrarCapacidad ();
    }

    public static boolean isNumeric(String value) {

        boolean ret = true;
        try {
            Double.parseDouble ( value );
        } catch (NumberFormatException e) {
            ret = false;
        }
        return ret;
    }
}
